window.onload = function() {
  const options = {
    "interaction": {
      "hover": true,
      "multiselect": true
    },
    "layout": {
      "improvedLayout": true,
      "randomSeed": 1
    },
    "autoResize": true,
    "edges": {
      "hoverWidth": 2,
      "physics": true,
      "font": {
        "align": "horizontal",
        "size": 25,
        "face": "Arial"
      },
      "smooth": {
        "roundness": 0.25,
        "forceDirection": "vertical",
        "type": "diagonalCross"
      },
      "labelHighlightBold": true
    },
    "manipulation": false,
    "nodes": {
      "scaling": {
        "max": 30,
        "label": {
          "max": 75,
          "maxVisible": 30,
          "enabled": true,
          "drawThreshold": 5,
          "min": 25
        },
        "min": 10
      },
      "shape": "box",
      "font": {
        "color": "white",
        "align": "center",
        "size": 25,
        "face": "Arial"
      },
      "physics": true,
      "size": 150
    },
    "physics": {
      "hierarchicalRepulsion": {
        "nodeDistance": 500,
        "springLength": 190,
        "centralGravity": 0
      },
      "minVelocity": 15,
      "solver": "hierarchicalRepulsion"
    }
  };

  const parsed = vis.network.convertDot(document.getElementById("graph").dataset.dotstring);

  for (let i = 0; i < 2; i++) {
    const elements = parsed[['nodes', 'edges'][i]];
    for (let j = 0; j < elements.length; j++) {
      // https://github.com/almende/vis/issues/3015
      elements[j].label = elements[j].label.replace(/\\n/g, '\n');
    }
  }

  // gprof2dot sets very small font sizes on nodes and edges.
  // Remove those to let vis take care of it.
  for (let node of parsed.nodes) {
    if (node.font && node.font.size) {
      delete node.font.size;
    }
  }
  for (let edge of parsed.edges) {
    if (edge.font && edge.font.size) {
      delete edge.font.size;
    }
  }

  const data = {
    nodes: new vis.DataSet(parsed.nodes),
    edges: new vis.DataSet(parsed.edges)
  };

  const graph = document.getElementById('graph');
  const network = new vis.Network(graph, data, options);
  network.on('stabilized', function () {
    document.getElementById('spinner').hidden = true;
    document.getElementById('freeze').hidden = true;
  });
  network.on('startStabilizing', function () {
    document.getElementById('freeze').hidden = false;
  });
  document.getElementById('freeze').onclick = function () {
    network.stopSimulation();
  };

  const flamegraph = document.getElementById('flamegraph');
  const showGraph = document.getElementById('show-graph');
  const showFlamegraph = document.getElementById('show-flamegraph');
  showGraph.hidden = true;
  flamegraph.hidden = true;

  showFlamegraph.onclick = function () {
    graph.hidden = true;
    flamegraph.hidden = false;

    showFlamegraph.hidden = true;
    showGraph.hidden = false;
  };

  showGraph.onclick = function () {
    flamegraph.hidden = true;
    graph.hidden = false;

    showGraph.hidden = true;
    showFlamegraph.hidden = false;
  };

  document.onkeydown = function (e) {
    e = e || window.event;
    if (e.key.toLowerCase() === "s") {
      network.stopSimulation();
    }
  };
};
